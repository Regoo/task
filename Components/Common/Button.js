import React from 'react';
import { TouchableOpacity, Text, View, StyleSheet } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
const Button = props => {
  return (
    <LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']} >
    <TouchableOpacity style={styles.button} onPress={props.onPress}>
      <Text style={styles.buttonTex}>{props.children}</Text>
      
    </TouchableOpacity>
    </LinearGradient>
  );
};
const styles = StyleSheet.create({
  button: {
    marginBottom:10,
    marginTop:70,
    marginLeft:10,
    marginRight:10,
    justifyContent: 'center',
    alignItems:'center',
    flex: 1,
    backgroundColor: 'transparent'
    
    //backgroundColor:'#F6813A'
  },

  buttonTex: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 15,
    fontWeight: 'bold',
  },
});

export default Button;
