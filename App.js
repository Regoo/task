/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import * as React from 'react';
import { Text, View , AppRegistry } from 'react-native';
//import Constants from 'expo-constants';

// import from local files
import Home from './Components/Home';
import LoginForm from './Components/LoginForm';
import RootNavigator from './Components/RootNavigator';
import App1 from './Components/App1';


export default App1;
AppRegistry.registerComponent('project1',()=>App1);


